﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class KhachHangBUS
    {
        private KhachHangDAO khachHangDAO;

        public KhachHangBUS()
        {
            khachHangDAO = new KhachHangDAO();
        }

        public List<KhachHangDTO> LayDanhSach()
        {
            return khachHangDAO.LayDanhSach();
        }

        public bool ThemKhachHang(string makh, string tenkh, string ngaysinh, string gioitinh, string cmnd,
                                string diachi, string sdt, string tendn)
        {
            return khachHangDAO.ThemKhachHang(makh, tenkh, ngaysinh, gioitinh, cmnd, sdt, diachi, tendn);
        }

        public void XoaKhachHang(string mnv)
        {
            khachHangDAO.XoaKhachHang(mnv);
        }

        public int SoLuongKhachHang()
        {
            return khachHangDAO.SoLuongKhachHang();
        }
    }
}
