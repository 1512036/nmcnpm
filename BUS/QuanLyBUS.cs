﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class QuanLyBUS
    {
        private QuanLyDAO quanLyDAO;

        public QuanLyBUS()
        {
            quanLyDAO = new QuanLyDAO();
        }

        public List<QuanLyDTO> LayDanhSach()
        {
            return quanLyDAO.LayDanhSach();
        }

        public QuanLyDTO LayThongTin(string tendangnhap)
        {
            return quanLyDAO.LayThongTin(tendangnhap);
        }

        public bool ThemQuanLy(string maql, string tendangnhap, string email, string hoten, string ngaysinh,
          string CMND, string gioitinh, string sodienthoai, string diachi)
        {
            return quanLyDAO.ThemQuanLy1(maql, tendangnhap, email, hoten, ngaysinh, CMND, gioitinh, sodienthoai, diachi);
        }

        public void XoaQuanLy(string maql)
        {
            quanLyDAO.XoaQuanLy(maql);
        }
    }
}
