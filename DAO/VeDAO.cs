﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class VeDAO
    {
        public List<VeDTO> LayDanhSach()
        {
            List<VeDTO> listVeDTO = new List<VeDTO>();

            String query = "SELECT * FROM Ve";
            DataTable dt = DataProvider.ExecuteQuery(query);
            foreach (DataRow dr in dt.Rows)
            {
                VeDTO VeDTO = new VeDTO();
                VeDTO.MaKhachHang = dr["MaKhachHang"].ToString();
                VeDTO.MaVe = dr["MaVe"].ToString();
                VeDTO.LoaiVe = dr["LoaiVe"].ToString();
                VeDTO.MaPhim = dr["MaPhim"].ToString();
                VeDTO.TenPhim = dr["TenPhim"].ToString();
                VeDTO.NgayChieu = dr["NgayChieu"].ToString();
                VeDTO.MaPhongChieu = dr["MaPhongChieu"].ToString();
                VeDTO.ViTriNgoi = dr["ViTriNgoi"].ToString();
                VeDTO.Gia = Convert.ToInt32(dr["Gia"]);

                listVeDTO.Add(VeDTO);
            }

            return listVeDTO;
        }
    }
}
