﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class KhachHangDAO
    {
        public List<KhachHangDTO> LayDanhSach()
        {
            List<KhachHangDTO> listKhachHangDTO = new List<KhachHangDTO>();

            String query = "SELECT * FROM KhachHang";
            DataTable dt = DataProvider.ExecuteQuery(query);
            foreach (DataRow dr in dt.Rows)
            {
                KhachHangDTO khachHangDTO = new KhachHangDTO();
                khachHangDTO.MaKhachHang = dr["MaKhachHang"].ToString();
                khachHangDTO.TenDangNhap = dr["TenDangNhap"].ToString();
                khachHangDTO.TenKhachHang = dr["TenKhachHang"].ToString();
                khachHangDTO.NgaySinh = (dr["NgaySinh"]).ToString();
                khachHangDTO.GioiTinh = dr["GioiTinh"].ToString();
                khachHangDTO.CMND = dr["CMND"].ToString();
                khachHangDTO.SoDienThoai = dr["SoDienThoai"].ToString();
                khachHangDTO.DiaChi = dr["DiaChi"].ToString();

                listKhachHangDTO.Add(khachHangDTO);
            }
            return listKhachHangDTO;
        }

        public bool ThemKhachHang(string makh, string tenkh, string ngaysinh, string gioitinh, string cmnd,
                                string diachi, string sdt, string tendn)
        {
            String test_makh = "SELECT * FROM KhachHang WHERE KhachHang.MaKhachHang = '" + makh + "'";
            DataTable dt_makh = DataProvider.ExecuteQuery(test_makh);
            if (dt_makh.Rows.Count > 0)
                return false;
            String query = @"INSERT INTO KhachHang VALUES ('" + makh + "', N'" + tenkh + "', '" + ngaysinh + "', N'" + gioitinh + "', '"
                + cmnd + "',N'" + diachi + "', '" + sdt + "', '" + tendn + "')";
            DataProvider.ExecuteQuery(query);
            return true;
        }

        public void XoaKhachHang(string makh)
        {
            String query = "DELETE FROM KhachHang WHERE MaKhachHang = '" + makh + "' ";
            DataProvider.ExecuteQuery(query);
        }

        public int SoLuongKhachHang()
        {
            int count = 0;
            String query = "SELECT count(KhachHang.MaKhachHang) as SoLuong FROM KhachHang";
            DataTable dt = DataProvider.ExecuteQuery(query);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                count = Convert.ToInt32(dr["SoLuong"].ToString());
            }
            return count;
        }


    }
}
