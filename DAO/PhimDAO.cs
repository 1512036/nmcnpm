﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class PhimDAO
    {
        public List<PhimDTO> LayDanhSach()
        {
            List<PhimDTO> listPhimDTO = new List<PhimDTO>();

            String query = "SELECT * FROM Phim";
            DataTable dt = DataProvider.ExecuteQuery(query);
            foreach (DataRow dr in dt.Rows)
            {
                PhimDTO phimDTO = new PhimDTO();
                phimDTO.MaPhim = dr["MaPhim"].ToString();
                phimDTO.TenPhim = dr["TenPhim"].ToString();
                phimDTO.TheLoai = dr["TheLoai"].ToString();
                phimDTO.DaoDien = dr["DaoDien"].ToString();
                phimDTO.DienVien = dr["DienVien"].ToString();
                phimDTO.GioiHanDoTuoi = dr["GioiHanDoTuoi"].ToString();
                phimDTO.NoiDung = dr["NoiDung"].ToString();
                phimDTO.NamSanXuat = Convert.ToInt32(dr["NamSanXuat"]);
                phimDTO.Poster = dr["Poster"].ToString();
                phimDTO.Trailer = dr["Trailer"].ToString();

                listPhimDTO.Add(phimDTO);
            }

            return listPhimDTO;
        }

        public PhimDTO TimKiemTheoTen(string tenphim)
        {
            PhimDTO phimDTO = null;
            String query = "SELECT * FROM Phim WHERE TenPhim LIKE N'%" + tenphim + "%'";
            DataTable dt = DataProvider.ExecuteQuery(query);
            if (dt.Rows.Count > 0)
            {
                phimDTO = new PhimDTO();
                phimDTO.MaPhim = dt.Rows[0]["MaPhim"].ToString();
                phimDTO.TenPhim = dt.Rows[0]["TenPhim"].ToString();
                phimDTO.TheLoai = dt.Rows[0]["TheLoai"].ToString();
                phimDTO.DaoDien = dt.Rows[0]["DaoDien"].ToString();
                phimDTO.DienVien = dt.Rows[0]["DienVien"].ToString();
                phimDTO.GioiHanDoTuoi = dt.Rows[0]["GioiHanDoTuoi"].ToString();
                phimDTO.NoiDung = dt.Rows[0]["NoiDung"].ToString();
                phimDTO.NamSanXuat = Convert.ToInt32(dt.Rows[0]["NamSanXuat"]);
                phimDTO.Poster = dt.Rows[0]["Poster"].ToString();
                phimDTO.Trailer = dt.Rows[0]["Trailer"].ToString();
            }
            return phimDTO;
        }

        public List<PhimDTO> TimKiemTheoTheLoai(string theloai)
        {
            List<PhimDTO> listPhimDTO = new List<PhimDTO>();

            String query = "SELECT * FROM Phim WHERE TheLoai = N'" + theloai + "' ";
            DataTable dt = DataProvider.ExecuteQuery(query);
            foreach (DataRow dr in dt.Rows)
            {
                PhimDTO phimDTO = new PhimDTO();
                phimDTO.MaPhim = dr["MaPhim"].ToString();
                phimDTO.TenPhim = dr["TenPhim"].ToString();
                phimDTO.TheLoai = dr["TheLoai"].ToString();
                phimDTO.DaoDien = dr["DaoDien"].ToString();
                phimDTO.DienVien = dr["DienVien"].ToString();
                phimDTO.GioiHanDoTuoi = dr["GioiHanDoTuoi"].ToString();
                phimDTO.NoiDung = dr["NoiDung"].ToString();
                phimDTO.NamSanXuat = Convert.ToInt32(dr["NamSanXuat"]);
                phimDTO.Poster = dr["Poster"].ToString();
                phimDTO.Trailer = dr["Trailer"].ToString();

                listPhimDTO.Add(phimDTO);
            }
            return listPhimDTO;
        }

        public bool ThemPhim(string maphim, string tenphim, string theloai, string daodien, string dienvien,
                               string gioihandotuoi, string noidung, int namsx, string poster, string trailer)
        {
            String test_maphim = "SELECT * FROM Phim WHERE Phim.MaPhim = '" + maphim + "'";
            DataTable dt_maphim = DataProvider.ExecuteQuery(test_maphim);
            if (dt_maphim.Rows.Count > 0)
                return false;
            String query = @"INSERT INTO Phim VALUES ('" + maphim + "', N'" + tenphim + "', N'" + theloai + "', N'" + daodien + "', N'"
                + dienvien + "',N'" + gioihandotuoi + "', N'" + noidung + "', " + namsx + " , '" + poster + "', '" + trailer + "')";
            DataProvider.ExecuteQuery(query);
            return true;
        }

        public void XoaPhim(string maphim)
        {
            String query = "DELETE FROM Phim WHERE MaPhim = '" + maphim + "' ";
            DataProvider.ExecuteQuery(query);
        }
    }
}