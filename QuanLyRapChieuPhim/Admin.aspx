﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site_QL.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="QuanLyRapChieuPhim.Admin" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

   
    <h1 style="text-align:center">
        Xin chào Admin
        <asp:label runat="server" Font-Bold="true" ForeColor="Orange" ID="lb_TenDangNhap"></asp:label>
    </h1>
    <div style="width: 100%;">
        <div style="float:left">
            <p style="text-align:left; font-family:'Segoe UI'; font-size:larger; padding:4cm; height: 0px;">
                <strong>THÔNG TIN</strong><br /><br />
                <strong>ID:</strong>&nbsp;&nbsp;
                <asp:Label ID="lb_ID" runat="server"></asp:Label><br />
                <strong>Họ tên:</strong>&nbsp;&nbsp;
                <asp:Label ID="lb_HoTen" runat="server"></asp:Label><br />
                <strong>Ngày sinh:</strong>&nbsp;&nbsp;
                <asp:Label ID="lb_NgaySinh" runat="server"></asp:Label><br />
                <strong>Giới tính:</strong>&nbsp;&nbsp;
                <asp:Label ID="lb_GioiTinh" runat="server"></asp:Label><br />
                <strong>CMND:</strong>&nbsp;&nbsp;
                <asp:Label ID="lb_CMND" runat="server"></asp:Label><br />
                <strong>Số điện thoại:</strong>&nbsp;&nbsp;
                <asp:Label ID="lb_SDT" runat="server"></asp:Label><br />
                <strong>Địa chỉ:</strong>&nbsp;&nbsp;
                <asp:Label ID="lb_DiaChi" runat="server"></asp:Label><br />
                <strong>Email:</strong>&nbsp;&nbsp;
                <asp:Label ID="lb_Email" runat="server"></asp:Label><br />
            </p>
        </div>
        <div style="float:right;" runat="server">
            <p style="text-align:left; font-family:'Segoe UI'; font-size:larger; padding:4cm">
                <strong>CHỨC NĂNG</strong><br /><br />
                <a href="QuanLyQuanLy.aspx">Quản lý quản lý</a><br />
                <a href="QuanLyNhanVien.aspx">Quản lý nhân viên</a><br />
                <a href="QuanLyKhachHang.aspx">Quản lý khách hàng</a><br />
                <a href="QuanLyPhongChieu.aspx">Quản lý phòng chiếu</a><br />
                <a href="QuanLyPhim.aspx">Quản lý phim</a><br />
                <a href="QuanLyVe.aspx">Quản lý vé</a><br />
            </p>
        </div>
    </div>
    <div style="clear:both"></div>
    
</asp:Content>
