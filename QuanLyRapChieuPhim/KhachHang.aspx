﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="KhachHang.aspx.cs" Inherits="QuanLyRapChieuPhim.KhachHang" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!DOCTYPE html>

    <style type="text/css">
        #form1 {
            margin-left: 40px;
        }
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 137px;
            color: #FF0000;
            font-size: large;
        }
        .auto-style3 {
            width: 137px;
            height: 23px;
            color: #FF0000;
            font-size: large;
        }
        .auto-style4 {
            height: 23px;
            font-size: large;
        }
        .auto-style5 {
            text-align: right;
        }
        .auto-style7 {
            color: #FF0000;
            font-size: large;
        }
        .auto-style8 {
            width: 137px;
            height: 22px;
            color: #FF0000;
            font-size: large;
        }
        .auto-style9 {
            height: 22px;
        }
        .auto-style10 {
            color: #FF0000;
            font-size: x-large;
        }
        .auto-style11 {
            width: 137px;
            color: #FF0000;
            height: 21px;
            font-size: large;
        }
        .auto-style12 {
            height: 21px;
        }
        .auto-style13 {
            font-size: large;
        }
        .auto-style14 {
            width: 137px;
            color: #FF0000;
            font-size: large;
            height: 27px;
        }
        .auto-style15 {
            height: 27px;
        }
    </style>
<body style="font-weight: 700">
        <p style="text-align: center">
            <span class="auto-style10">Xin chào Quý khách</span><asp:Label ID="LB_TKH" runat="server"></asp:Label>
        </p>
        <p style="text-align: left" class="auto-style7">
            Thông tin:</p>
        <p class="auto-style5">
            <asp:Label ID="LB_DX" runat="server" Font-Overline="False" Font-Underline="True" ForeColor="#0066CC" Text="Đăng Xuất" CssClass="auto-style13"></asp:Label>
        </p>
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">ID:</td>
                <td class="auto-style13">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">Tên đăng nhập:</td>
                <td class="auto-style13">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">Mật khẩu:</td>
                <td class="auto-style13">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Email:</td>
                <td class="auto-style4">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style8">Họ tên:</td>
                <td class="auto-style9">
                    </td>
            </tr>
            <tr>
                <td class="auto-style11">Ngày sinh:</td>
                <td class="auto-style12">
                    </td>
            </tr>
            <tr>
                <td class="auto-style2">Giới tính:</td>
                <td class="auto-style13">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style14">CMND:</td>
                <td class="auto-style15">
                    </td>
            </tr>
            <tr>
                <td class="auto-style2">SĐT:</td>
                <td class="auto-style13">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">Địa chỉ:</td>
                <td class="auto-style13">
                    &nbsp;</td>
            </tr>
        </table>
</body>
</asp:Content>
