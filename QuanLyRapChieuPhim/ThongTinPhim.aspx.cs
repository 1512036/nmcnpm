﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuanLyRapChieuPhim
{
    public class ListPhim
    {
        public static List<PhimDTO> val_Loai;
        public static PhimDTO val_Ten;
        public static int len = 0;
        public static int index = 0;
    }

    public partial class ThongTinPhim : System.Web.UI.Page
    {
        bool isSearchName;
        protected void Page_Load(object sender, EventArgs e)
        {
            isSearchName = (bool)Session["Index"];
            PhimBUS phimBUS = new PhimBUS();
            if (isSearchName)
            {
                ListPhim.val_Ten = phimBUS.TimKiemTheoTen(Session["TenPhim"].ToString());
                lbl_error.Text = "Không tìm thấy tên phim bạn nhập" + ListPhim.index;
                lnk_back.Visible = true;
                lnk_forward.Visible = false;
            }
            else
            {
                ListPhim.val_Loai = phimBUS.TimKiemTheoTheLoai(Session["TheLoai"].ToString());
                ListPhim.len = ListPhim.val_Loai.Count;
                lbl_error.Text = "Không tìm thấy phim nào thuộc thể loại " + Session["TheLoai"].ToString();
                if (ListPhim.index == ListPhim.len - 1 || ListPhim.len == 1)
                {
                    lnk_back.Visible = true;
                    lnk_forward.Visible = false;
                }
                else
                {
                    lnk_back.Visible = true;
                    lnk_forward.Visible = true;
                } 
            }
            if (ListPhim.len == 0 || ListPhim.val_Loai[0] == null)
            {
                lbl_error.Visible = true;
                img_Phim.Visible = false;
                btn_DatVe.Visible = false;
                btn_Trailer.Visible = false;
                lbl_NoiDung.Visible = false;
            }
            else
            {
                lbl_error.Visible = false;
                img_Phim.Visible = true;
                btn_DatVe.Visible = true;
                btn_Trailer.Visible = true;
                lbl_NoiDung.Visible = true;

                if (isSearchName)
                {
                    img_Phim.ImageUrl = ListPhim.val_Ten.Poster;
                    img_Phim.DataBind();
                    lbl_NoiDung.Text = ListPhim.val_Ten.NoiDung;
                }
                else
                {
                    img_Phim.ImageUrl = ListPhim.val_Loai[ListPhim.index].Poster;
                    img_Phim.DataBind();
                    lbl_NoiDung.Text = ListPhim.val_Loai[ListPhim.index].NoiDung;
                }
            }
        }

        protected void btn_DatVe_Click(object sender, EventArgs e)
        {
            if (isSearchName)
                Session["TenPhim"] = ListPhim.val_Ten.TenPhim;
            else
                Session["TenPhim"] = ListPhim.val_Loai[ListPhim.index].TenPhim;
            Response.Redirect("DatVe.aspx");
        }

        protected void lnk_back_Click(object sender, EventArgs e)
        {
            if (isSearchName || ListPhim.index == 0)
                Response.Redirect("PhimDangChieu.aspx");
            else
            {
                ListPhim.index--;
                Response.Redirect("ThongTinPhim.aspx");
            }
        }

        protected void lnk_forward_Click(object sender, EventArgs e)
        {
            ListPhim.index++;
            Response.Redirect("ThongTinPhim.aspx");
        }

        protected void btn_Trailer_Click(object sender, EventArgs e)
        {
            if (isSearchName)
                Response.Redirect(ListPhim.val_Ten.Trailer);
            else
                Response.Redirect(ListPhim.val_Loai[ListPhim.index].Trailer);
        }
    }
}