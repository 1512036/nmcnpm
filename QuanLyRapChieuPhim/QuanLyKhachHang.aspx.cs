﻿using BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuanLyRapChieuPhim
{
    public partial class QuanLyKhachHang : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                FilGVDanhSachKhachHang();

        }

        private void FilGVDanhSachKhachHang()
        {
            KhachHangBUS khachHangBUS = new KhachHangBUS();
            gvDanhSachKhachHang.DataSource = khachHangBUS.LayDanhSach();
            gvDanhSachKhachHang.DataBind();

        }

        protected void gvDanhSachKhachHang_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            TaiKhoanBUS tkBUS = new TaiKhoanBUS();
            KhachHangBUS nvBUS = new KhachHangBUS();
            TableCell cell = gvDanhSachKhachHang.Rows[e.RowIndex].Cells[0];
            nvBUS.XoaKhachHang(cell.Text);
            cell = gvDanhSachKhachHang.Rows[e.RowIndex].Cells[8];
            tkBUS.XoaTaiKhoan(cell.Text);
            FilGVDanhSachKhachHang();
        }
    }
}