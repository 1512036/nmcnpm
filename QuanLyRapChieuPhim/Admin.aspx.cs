﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuanLyRapChieuPhim
{
    public partial class Admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String tendangnhap = Session["TenDangNhap"].ToString();
            AdminBUS adminBUS = new AdminBUS();
            AdminDTO adminDTO = adminBUS.LayThongTin(tendangnhap);
            lb_TenDangNhap.Text = tendangnhap;

            lb_ID.Text = adminDTO.AdminID;
            lb_HoTen.Text = adminDTO.HoTen;
            lb_NgaySinh.Text = adminDTO.NgaySinh;
            lb_GioiTinh.Text = adminDTO.GioiTinh;
            lb_CMND.Text = adminDTO.CMND;
            lb_SDT.Text = adminDTO.SoDienThoai;
            lb_DiaChi.Text = adminDTO.DiaChi;
            lb_Email.Text = adminDTO.Email;
        }
    }
}