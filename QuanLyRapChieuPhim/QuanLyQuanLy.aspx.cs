﻿using BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuanLyRapChieuPhim
{
    public partial class QuanLyQuanLy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                FilGVDanhSachQuanLy();
        }

        private void FilGVDanhSachQuanLy()
        {
            QuanLyBUS quanLyBUS = new QuanLyBUS();
            gvDanhSachQuanLy.DataSource = quanLyBUS.LayDanhSach();
            gvDanhSachQuanLy.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            bool flag_tk = false;
            TaiKhoanBUS tkBUS = new TaiKhoanBUS();
            flag_tk = tkBUS.ThemTaiKhoan(tbTenDN.Text, tbMatKhau.Text, "QL");
            if (flag_tk == false)
            {
                string strBuilder = "<script language='javascript'>alert('" + "Ten Dang Nhap da ton tai" + "')</script>";
                Response.Write(strBuilder);
                tbMaQL.Text = "";
                tbTenQL.Text = "";
                tbGioiTinh.Text = "";
                tbNgaySinh.Text = "";
                tbCMND.Text = "";
                tbSDT.Text = "";
                tbDiaChi.Text = "";
                tbTenDN.Text = "";
                tbMatKhau.Text = "";
                tbEmail.Text = "";
                return;
            }
            bool flag_nv = false;
            QuanLyBUS nvBUS = new QuanLyBUS();
            flag_nv = nvBUS.ThemQuanLy(tbMaQL.Text, tbTenDN.Text, tbEmail.Text, tbTenQL.Text, tbNgaySinh.Text, tbGioiTinh.Text, tbCMND.Text,
                                 tbSDT.Text, tbDiaChi.Text);
            if (flag_nv == false)
            {
                string strBuilder = "<script language='javascript'>alert('" + "Ma Quan Ly da ton tai" + "')</script>";
                Response.Write(strBuilder);
                tbMaQL.Text = "";
                tbTenQL.Text = "";
                tbGioiTinh.Text = "";
                tbNgaySinh.Text = "";
                tbCMND.Text = "";
                tbSDT.Text = "";
                tbDiaChi.Text = "";
                tbTenDN.Text = "";
                tbMatKhau.Text = "";
                tbEmail.Text = "";
                return;
            }
            tbMaQL.Text = "";
            tbTenQL.Text = "";
            tbGioiTinh.Text = "";
            tbNgaySinh.Text = "";
            tbCMND.Text = "";
            tbSDT.Text = "";
            tbDiaChi.Text = "";
            tbTenDN.Text = "";
            tbMatKhau.Text = "";
            tbEmail.Text = "";
            FilGVDanhSachQuanLy();
        }

        protected void gvDanhSachQuanLy_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            TaiKhoanBUS tkBUS = new TaiKhoanBUS();
            QuanLyBUS nvBUS = new QuanLyBUS();
            TableCell cell = gvDanhSachQuanLy.Rows[e.RowIndex].Cells[0];
            nvBUS.XoaQuanLy(cell.Text);
            cell = gvDanhSachQuanLy.Rows[e.RowIndex].Cells[8];
            tkBUS.XoaTaiKhoan(cell.Text);
            FilGVDanhSachQuanLy();
        }
    }
}