﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuanLyRapChieuPhim
{
    public partial class QuanLy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String tendangnhap = Session["TenDangNhap"].ToString();
            QuanLyBUS quanLyBUS = new QuanLyBUS();
            QuanLyDTO quanLyDTO = quanLyBUS.LayThongTin(tendangnhap);
            lb_TenDangNhap.Text = tendangnhap;

            lb_ID.Text = quanLyDTO.MaQuanLy;
            lb_HoTen.Text = quanLyDTO.HoTen;
            lb_NgaySinh.Text = quanLyDTO.NgaySinh;
            lb_GioiTinh.Text = quanLyDTO.GioiTinh;
            lb_CMND.Text = quanLyDTO.CMND;
            lb_SDT.Text = quanLyDTO.SoDienThoai;
            lb_DiaChi.Text = quanLyDTO.DiaChi;
            lb_Email.Text = quanLyDTO.Email;
        }
    }
}