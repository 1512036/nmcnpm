﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuanLyRapChieuPhim
{
    public partial class PhimDangChieu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BT_Login_Click(object sender, EventArgs e)
        {

        }

        protected void btn_search_name_Click(object sender, ImageClickEventArgs e)
        {
            Session["TenPhim"] = txt_name.Text;
            Session["Index"] = true;
            Response.Redirect("ThongTinPhim.aspx");
        }

        protected void btn_search_category_Click(object sender, ImageClickEventArgs e)
        {
            Session["TheLoai"] = DropDownList1.SelectedItem.Text;
            Session["Index"] = false;
            Response.Redirect("ThongTinPhim.aspx");
        }
    }
}