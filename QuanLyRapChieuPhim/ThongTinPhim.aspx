﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ThongTinPhim.aspx.cs" Inherits="QuanLyRapChieuPhim.ThongTinPhim" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:LinkButton ID="lnk_back" OnClick="lnk_back_Click" runat="server">QUAY LẠI</asp:LinkButton>
    &nbsp; |&nbsp;
    <asp:LinkButton ID="lnk_forward" OnClick="lnk_forward_Click" runat="server">TIẾP TỤC</asp:LinkButton><br /><br />
    <asp:Label ID="lbl_error" runat="server"></asp:Label>
    <p style ="text-align:center">
        <asp:Image ID="img_Phim"  runat="server" /><br />
        <asp:Button ID="btn_DatVe" OnClick="btn_DatVe_Click" runat="server" Text="Đặt Vé" />
        <asp:Button ID="btn_Trailer" OnClick="btn_Trailer_Click" runat="server" Text="Trailer" /><br /><br />
        NỘI DUNG PHIM
        <asp:Label ID="lbl_NoiDung" runat="server"></asp:Label>
    </p>
</asp:Content>
