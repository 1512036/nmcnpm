﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuanLyRapChieuPhim
{
    public partial class NhanVien : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String tendangnhap = Session["TenDangNhap"].ToString();
            NhanVienBUS nhanVienBUS = new NhanVienBUS();
            NhanVienDTO nhanVienDTO = nhanVienBUS.LayThongTin(tendangnhap);
            lb_TenDangNhap.Text = tendangnhap;

            lb_ID.Text = nhanVienDTO.MaNhanVien;
            lb_HoTen.Text = nhanVienDTO.TenNhanVien;
            lb_NgaySinh.Text = nhanVienDTO.NgaySinh;
            lb_GioiTinh.Text = nhanVienDTO.GioiTinh;
            lb_CMND.Text = nhanVienDTO.CMND;
            lb_SDT.Text = nhanVienDTO.SoDienThoai;
            lb_DiaChi.Text = nhanVienDTO.DiaChi;
            lb_Email.Text = nhanVienDTO.Email;
            lb_ViTri.Text = nhanVienDTO.ViTri;
        }
    }
}