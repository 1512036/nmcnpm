﻿using BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuanLyRapChieuPhim
{
    public partial class QuanLyPhim : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                FilGVDanhSachPhim();
        }

        private void FilGVDanhSachPhim()
        {
            PhimBUS phimBUS = new PhimBUS();
            gvDanhSachPhim.DataSource = phimBUS.LayDanhSach();
            gvDanhSachPhim.DataBind();
        }

        protected void btnAddPhim_Click(object sender, EventArgs e)
        {
            bool flag_nv = false;
            PhimBUS pBUS = new PhimBUS();
            flag_nv = pBUS.ThemPhim(tbMaPhim.Text, tbTenPhim.Text, tbTheLoai.Text, tbDaoDien.Text, tbDienVien.Text,
                                tbNoiDung.Text, tbGHDT.Text, Convert.ToInt32(tbNamSX.Text), tbPoster.Text, tbTrailer.Text);
            if (flag_nv == false)
            {
                string strBuilder = "<script language='javascript'>alert('" + "Ma Phim da ton tai" + "')</script>";
                Response.Write(strBuilder);
                tbMaPhim.Text = "";
                tbTenPhim.Text = "";
                tbDaoDien.Text = "";
                tbTheLoai.Text = "";
                tbDienVien.Text = "";
                tbGHDT.Text = "";
                tbNoiDung.Text = "";
                tbNamSX.Text = "";
                tbPoster.Text = "";
                tbTrailer.Text = "";

                return;
            }
            tbMaPhim.Text = "";
            tbTenPhim.Text = "";
            tbDaoDien.Text = "";
            tbTheLoai.Text = "";
            tbDienVien.Text = "";
            tbGHDT.Text = "";
            tbNoiDung.Text = "";
            tbNamSX.Text = "";
            tbPoster.Text = "";
            tbTrailer.Text = "";
            FilGVDanhSachPhim();
        }

        protected void gvDanhSachPhim_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            PhimBUS pBUS = new PhimBUS();
            TableCell cell = gvDanhSachPhim.Rows[e.RowIndex].Cells[0];
            pBUS.XoaPhim(cell.Text);
            cell = gvDanhSachPhim.Rows[e.RowIndex].Cells[8];
            FilGVDanhSachPhim();
        }
    }
}